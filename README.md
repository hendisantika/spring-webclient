# Spring Boot WebClient

### What Is the WebClient?

Simply put, WebClient is an interface representing the main entry point for performing web requests.

It was created as part of the Spring Web Reactive module and will be replacing the classic RestTemplate in these
scenarios. In addition, the new client is a reactive, non-blocking solution that works over the HTTP/1.1 protocol.

It's important to note that even though it is, in fact, a non-blocking client and it belongs to the spring-webflux
library, the solution offers support for both synchronous and asynchronous operations, making it suitable also for
applications running on a Servlet Stack.

This can be achieved by blocking the operation to obtain the result. Of course, this practice is not suggested if we're
working on a Reactive Stack.

Finally, the interface has a single implementation, the DefaultWebClient class, which we'll be working with.

### Working with the WebClient

In order to work properly with the client, we need to know how to:

* create an instance
* make a request
* handle the response

### Creating a WebClient Instance

There are three options to choose from. The first one is creating a WebClient object with default settings:

```
WebClient client = WebClient.create();
```

The second option is to initiate a WebClient instance with a given base URI:

```
WebClient client = WebClient.create("http://localhost:8080");
```

The third option (and the most advanced one) is building a client by using the DefaultWebClientBuilder class, which
allows full customization:

```java
WebClient client=WebClient.builder()
        .baseUrl("http://localhost:8080")
        .defaultCookie("cookieKey","cookieValue")
        .defaultHeader(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
        .defaultUriVariables(Collections.singletonMap("url","http://localhost:8080"))
        .build();
```

### Creating a WebClient Instance with Timeouts

Oftentimes, the default HTTP timeouts of 30 seconds are too slow for our needs, to customize this behavior, we can
create an `HttpClient` instance and configure our `WebClient` to use it.

We can:

* set the connection timeout via the ChannelOption.CONNECT_TIMEOUT_MILLIS option
* set the read and write timeouts using a ReadTimeoutHandler and a WriteTimeoutHandler, respectively
* configure a response timeout using the responseTimeout directive

* As we said, all these have to be specified in the `HttpClient` instance we'll configure:

```java
HttpClient httpClient=HttpClient.create()
        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS,5000)
        .responseTimeout(Duration.ofMillis(5000))
        .doOnConnected(conn->
        conn.addHandlerLast(new ReadTimeoutHandler(5000,TimeUnit.MILLISECONDS))
        .addHandlerLast(new WriteTimeoutHandler(5000,TimeUnit.MILLISECONDS)));

        WebClient client=WebClient.builder()
        .clientConnector(new ReactorClientHttpConnector(httpClient))
        .build();
```

Note that while we can call timeout on our client request as well, this is a signal timeout, not an HTTP connection, a
read/write, or a response timeout; it's a timeout for the Mono/Flux publisher.

### Making a Request

After building a WebTestClient object, all following operations in the chain are going to be similar to the WebClient
until the exchange method (one way to get a response), which provides the WebTestClient.ResponseSpec interface to work
with useful methods like the expectStatus, expectBody, and expectHeader:

```java
WebTestClient
        .bindToServer()
        .baseUrl("http://localhost:8080")
        .build()
        .post()
        .uri("/resource")
        .exchange()
        .expectStatus().isCreated()
        .expectHeader().valueEquals("Content-Type","application/json")
        .expectBody().jsonPath("field").isEqualTo("value");

```

Using https://jsonplaceholder.typicode.com/
