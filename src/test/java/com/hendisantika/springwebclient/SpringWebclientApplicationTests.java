package com.hendisantika.springwebclient;

import com.hendisantika.springwebclient.controller.SpringBootWebClientController;
import com.hendisantika.springwebclient.model.Post;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class SpringWebclientApplicationTests {

    @Mock
    private WebClient webClientMock;

    @Mock
    private WebClient.RequestBodyUriSpec requestBodyUriSpecMock;

    @Mock
    private WebClient.RequestBodySpec requestBodySpecMock;

    @SuppressWarnings("rawtypes")
    @Mock
    private WebClient.RequestHeadersSpec requestHeadersSpecMock;

    @SuppressWarnings("rawtypes")
    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriSpecMock;

    @Mock
    private WebClient.ResponseSpec responseSpecMock;

    @Mock
    private Mono<Post> postResponseMock;

    @InjectMocks
    private SpringBootWebClientController controllerMock;

    @SuppressWarnings("unchecked")
    @Test
    public void test_getPost() throws Exception {

        Post post = new Post(1001, 1001, "Some title", "Some body");

        when(webClientMock.get()).thenReturn(requestHeadersUriSpecMock);
        when(requestHeadersUriSpecMock.uri(anyString())).thenReturn(requestHeadersSpecMock);
        when(requestHeadersSpecMock.retrieve()).thenReturn(responseSpecMock);
        when(responseSpecMock.bodyToMono(
                ArgumentMatchers.<Class<Post>>notNull())).thenReturn(Mono.just(post));

        Mono<Post> response = controllerMock.getPost("1").getBody();
        Assertions.assertEquals(1001, response.block().getId());
        Assertions.assertEquals("Some title", response.block().getTitle());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_createPost() throws Exception {

        Post post = new Post(1002, 1002, "Some title", "Some body");

        when(webClientMock.post()).thenReturn(requestBodyUriSpecMock);
        when(requestBodyUriSpecMock.uri(anyString())).thenReturn(requestBodySpecMock);
        when(requestBodySpecMock.header(any(), any())).thenReturn(requestBodySpecMock);
        when(requestBodySpecMock.body(any())).thenReturn(requestHeadersSpecMock);
        when(requestHeadersSpecMock.retrieve()).thenReturn(responseSpecMock);
        when(responseSpecMock.bodyToMono(
                ArgumentMatchers.<Class<Post>>notNull())).thenReturn(Mono.just(post));

        Mono<Post> response = controllerMock.createPost(post);
        Assertions.assertEquals(1002, response.block().getId());
        Assertions.assertEquals("Some title", response.block().getTitle());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_updatePost() throws Exception {

        Post post = new Post(1003, 1003, "Some title", "Some body");

        when(webClientMock.put()).thenReturn(requestBodyUriSpecMock);
        when(requestBodyUriSpecMock.uri(anyString())).thenReturn(requestBodySpecMock);
        when(requestBodySpecMock.header(any(), any())).thenReturn(requestBodySpecMock);
        when(requestBodySpecMock.body(any())).thenReturn(requestHeadersSpecMock);
        when(requestHeadersSpecMock.retrieve()).thenReturn(responseSpecMock);
        when(responseSpecMock.bodyToMono(
                ArgumentMatchers.<Class<Post>>notNull())).thenReturn(Mono.just(post));

        Mono<Post> response = controllerMock.updatePost(post);
        Assertions.assertEquals(1003, response.block().getId());
        Assertions.assertEquals("Some title", response.block().getTitle());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_deletePost() throws Exception {

        Post post = new Post(1004, 1004, "Some title", "Some body");

        when(webClientMock.delete()).thenReturn(requestHeadersUriSpecMock);
        when(requestHeadersUriSpecMock.uri(anyString())).thenReturn(requestHeadersSpecMock);
        when(requestHeadersSpecMock.retrieve()).thenReturn(responseSpecMock);
        when(responseSpecMock.bodyToMono(
                ArgumentMatchers.<Class<Post>>notNull())).thenReturn(Mono.just(post));

        Mono<Post> response = controllerMock.deletePost("1");
        Assertions.assertEquals(1004, response.block().getId());
        Assertions.assertEquals("Some title", response.block().getTitle());
    }
}
