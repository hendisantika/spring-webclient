package com.hendisantika.springwebclient;

import com.hendisantika.springwebclient.model.Post;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webclient
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/02/21
 * Time: 11.16
 */
@SpringBootTest()
@ExtendWith(SpringExtension.class)
class SpringBootWebClientApplicationWebTestClientTests {

    @Autowired
    private static WebTestClient webTestClient;

    @BeforeAll
    public static void setup() {
        webTestClient = WebTestClient.bindToServer()
                .baseUrl("http://localhost:8080/microservice/webclient/v1")
                .build();
    }

    @Test
    public void test_getPost() throws Exception {
        Post post = new Post(1, 1,
                "Uzumaki Naruto",
                "Uzumaki Naruto Hokage");

        webTestClient.get().uri("/post/{id}", 1)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Post.class).isEqualTo(post);
    }

    @Test
    public void test_createPost() throws Exception {
        Post post = new Post(1, 101,
                "Uchiha Sasuke",
                "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut " +
                        "quas totam\nnostrum rerum est autem sunt rem eveniet architecto");

        webTestClient.post().uri("/post")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(post))
                .exchange()
                .expectStatus().isOk()
                .expectBody(Post.class).isEqualTo(post);
    }

    @Test
    public void test_updatePost() throws Exception {

        Post post = new Post(1003, 1, "Some title", "Some body");

        webTestClient.put().uri("/post")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(post))
                .exchange()
                .expectStatus().isOk()
                .expectBody(Post.class).isEqualTo(post);
    }

    @Test
    public void test_deletePost() throws Exception {

        Post post = new Post(0, 0, null, null);

        webTestClient.delete().uri("/post/{id}", 1)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Post.class).isEqualTo(post);
    }
}
