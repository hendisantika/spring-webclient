package com.hendisantika.springwebclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hendisantika.springwebclient.controller.SpringBootWebClientController;
import com.hendisantika.springwebclient.model.Post;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webclient
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/02/21
 * Time: 07.11
 */
@SpringBootTest()
@ExtendWith(SpringExtension.class)
class SpringBootWebClientApplicationMockWebServerTests {

    private static MockWebServer mockWebServer;

    @Autowired
    WebClient webclient;

    @Autowired
    private SpringBootWebClientController controller;

    @BeforeAll
    public static void setup() throws IOException {

        mockWebServer = new MockWebServer();
        mockWebServer.start(9090);
    }

    @AfterAll
    public static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void test_getPost() throws Exception {

        Post post = new Post(1001, 1001, "Some title", "Some body");

        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(post))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        ResponseEntity<Mono<Post>> response = controller.getPost("1");
        Assertions.assertEquals(1001, response.getBody().block().getId());
    }

    @Test
    public void test_createPost() throws Exception {

        Post post = new Post(1002, 1002, "Some title", "Some body");

        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(post))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        Post response = controller.createPost(post).block();
        // Check userId instead of id as id will be auto generated and sent back by the API
        Assertions.assertEquals(1002, response.getUserId());
    }

    @Test
    public void test_updatePost() throws Exception {

        Post post = new Post(1003, 1003, "Some title", "Some body");

        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(post))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        Post response = controller.updatePost(post).block();
        // Check userId instead of id as id will be auto generated and sent back by the API
        Assertions.assertEquals(1003, response.getUserId());
    }

    @Test
    public void test_deletePost() throws Exception {

        Post post = new Post(1004, 1004, "Some title", "Some body");

        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(post))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        Mono<Post> response = controller.deletePost("1");
        Assertions.assertEquals(1004, response.block().getId());
    }
}
