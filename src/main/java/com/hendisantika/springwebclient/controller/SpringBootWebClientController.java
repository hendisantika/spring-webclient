package com.hendisantika.springwebclient.controller;

import com.hendisantika.springwebclient.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webclient
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/02/21
 * Time: 05.25
 */
@RestController
@RequestMapping("/webclient")
public class SpringBootWebClientController {

    @Autowired
    private WebClient createWebClient;

    @SuppressWarnings({"unchecked", "rawtypes"})
    @GetMapping("/v1/post/{id}")
    public ResponseEntity<Mono<Post>> getPost(@PathVariable String id) {

        Mono<Post> postMono = createWebClient.get()
                .uri("/posts/" + id)
                .retrieve()
                .bodyToMono(Post.class);

        return new ResponseEntity(postMono, HttpStatus.OK);
    }

    @PostMapping(path = "/v1/post", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Post> createPost(@RequestBody Post post) {

        return createWebClient.post()
                .uri("/posts")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(BodyInserters.fromValue(post))
                .retrieve()
                .bodyToMono(Post.class);
    }

    @PutMapping(path = "/v1/post", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Post> updatePost(@RequestBody Post post) {

        return createWebClient.put()
                .uri("/posts/1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(BodyInserters.fromValue(post))
                .retrieve()
                .bodyToMono(Post.class);
    }

    @DeleteMapping(path = "/v1/post/{id}")
    public Mono<Post> deletePost(@PathVariable String id) {

        return createWebClient.delete()
                .uri("/posts/" + id)
                .retrieve()
                .bodyToMono(Post.class);
    }

}
