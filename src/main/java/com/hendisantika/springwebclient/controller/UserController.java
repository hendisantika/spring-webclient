package com.hendisantika.springwebclient.controller;

import com.hendisantika.springwebclient.model.Album;
import com.hendisantika.springwebclient.model.ToDo;
import com.hendisantika.springwebclient.model.User;
import com.hendisantika.springwebclient.model.UserResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple3;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webclient
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 6/4/23
 * Time: 06:36
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    @Autowired
    private final WebClient createWebClient;

    @GetMapping("/list")
    public Flux<User> getAllUsers() {
        return createWebClient.get()
                .uri("/users")
                .retrieve()
                .bodyToFlux(User.class);
    }

    @GetMapping("/todo")
    public Flux<ToDo> getAllUsersTodo() {
        return createWebClient.get()
                .uri("/todos")
                .retrieve()
                .bodyToFlux(ToDo.class);
    }

    @GetMapping("/album")
    public Flux<Album> getAllUsersAlbum() {
        return createWebClient.get()
                .uri("/albums")
                .retrieve()
                .bodyToFlux(Album.class);
    }

    @GetMapping("/merge")
    public Mono<UserResponse> mergeAPI(@RequestParam("userId") int userId) {
        Mono<User> userMono = createWebClient.get()
                .uri("/users/" + userId)
                .retrieve()
                .bodyToMono(User.class);

        Mono<ToDo> todoMono = createWebClient.get()
                .uri("/todos/" + userId)
                .retrieve()
                .bodyToMono(ToDo.class);

        Mono<Album> albumMono = createWebClient.get()
                .uri("/albums/" + userId)
                .retrieve()
                .bodyToMono(Album.class);

        UserResponse userResponse = new UserResponse();

        return Mono.zip(userMono, todoMono, albumMono)
                .map(this::combine);
    }

    private UserResponse combine(Tuple3<User, ToDo, Album> tuple) {
        return UserResponse.create(
                tuple.getT1(),
                tuple.getT2(),
                tuple.getT3()
        );
    }
}
