package com.hendisantika.springwebclient.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webclient
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 6/4/23
 * Time: 07:11
 * To change this template use File | Settings | File Templates.
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
public class UserResponse {
    private User user;
    private ToDo toDo;
    private Album album;
}
