package com.hendisantika.springwebclient.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webclient
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/02/21
 * Time: 05.24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {

    private int userId;

    private int id;

    private String title;

    private String body;
}